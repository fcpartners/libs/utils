package lngerr

import (
	ov "github.com/go-ozzo/ozzo-validation"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fcpartners/libs/utils"
	"google.golang.org/grpc/codes"
	//	"gitlab.com/fcpartners/apis/gen/order/v1/order"
	//	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"testing"
)

// TestLongErrorNoPanics in case several validation errors occurs.
// This test has invoked panic cause validation error isn't always ov.Errors.
func TestLongErrorNoPanics(t *testing.T) {
	// empty object
	var obj utils.Email
	ErrInvalidArgument := New("SV_INVALID_ARGUMENT", "invalid argument(s)").WithCode(codes.InvalidArgument)

	err := ErrInvalidArgument.FromValidation(ov.ValidateStruct(&obj,
		ov.Field(&obj.Username, ov.Required),
		ov.Field(&obj.Identity, ov.Required),
		ov.Field(&obj.Text, ov.Required),
	))
	assert.EqualError(t, err, ErrInvalidArgument.Error())

	err1 := ErrInvalidArgument.FromValidation(ov.Validate(&obj.Username, ov.Required))
	err2 := ErrInvalidArgument.FromValidation(ov.Validate(&obj.Text, ov.Required))

	assert.Equal(t, err1, ErrInvalidArgument)
	assert.EqualError(t, err1, ErrInvalidArgument.Error())
	assert.EqualError(t, err1, err2.Error())

	// check other validation error codes works properly
	ErrRequestRequired := New("ORDER_SV_REQUEST_REQUIRED", "request required").WithCode(codes.InvalidArgument)
	err = ErrRequestRequired.FromValidation(ov.Validate(nil, ov.NotNil))

	assert.Equal(t, err, ErrRequestRequired)
	assert.EqualError(t, err, ErrRequestRequired.Error())
}

//func TestToGRPC(t *testing.T) {
//	var ErrInvalidOrderStatus = New("INVALID_ORDER_STATUS", "invalid order status", "order has invalid status(%v)").WithCode(codes.FailedPrecondition)
//
//	ErrInvalidOrderStatus = ErrInvalidOrderStatus.WithParams(order.Order_DONE).WithInfo("inactive order status", "status", order.Order_DONE)
//	ctx := context.Background()
//	grpcErr := ErrInvalidOrderStatus.ToGRPC(ctx)
//
//	details := map[string][]string{}
//	details["code"] = []string{grpcErr.Code().String()}
//	for _, detail := range grpcErr.Details() {
//		switch t := detail.(type) {
//		case *errdetails.ErrorInfo:
//			details["domain"] = append(details["domain"], t.GetDomain())
//			details["reason"] = append(details["reason"], t.GetReason())
//			for k, v := range t.GetMetadata() {
//				details[k] = append(details[k], v)
//			}
//		case *errdetails.LocalizedMessage:
//			details["locale"] = append(details["locale"], t.GetLocale())
//			details["localized_message"] = append(details["localized_message"], t.GetMessage())
//		case *errdetails.PreconditionFailure:
//			for _, violation := range t.GetViolations() {
//				details[violation.Type] = append(details[violation.Type], violation.GetDescription(), violation.GetSubject())
//			}
//		case *errdetails.BadRequest:
//			for _, violation := range t.GetFieldViolations() {
//				details[violation.GetField()] = append(details[violation.GetField()], violation.GetDescription())
//			}
//		}
//	}
//
//	t.Log(grpcErr)
//	t.Log(details)
//}
