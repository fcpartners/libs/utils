package lngerr

import (
	"context"
	"fmt"
	"strings"

	ov "github.com/go-ozzo/ozzo-validation"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const DefaultLocale = "en_GB"
const Namespace = "ERRORS"

type TError string

func (t TError) String() string {
	return string(t)
}

var langFn func(ctx context.Context, key string) (locale string, msg string)

// SetLanguageFunc
// determine function who returns locale & translated message by key
// locale must extract from metadata of context
// if no present returns EMPTY locale. will be set default message without locale
func SetLanguageFunc(fn func(ctx context.Context, key string) (string, string)) {
	langFn = fn
}

var uniqueErrors = make(map[TError]string)

// ListErrors
// returns list of all error messages with their keys
func ListErrors() map[TError]string {
	m := make(map[TError]string, len(uniqueErrors))
	for k, v := range uniqueErrors {
		m[k] = v
	}
	return m
}

type Error interface {
	FromValidation(err error) Error
	WithCode(code codes.Code) Error
	WithInfo(reason string, metadata ...interface{}) Error

	Error() string
	ToGRPC(ctx context.Context) *status.Status
}

func New(key TError, message string) Error {
	if _, ok := uniqueErrors[key]; ok {
		panic(fmt.Sprintf("duplicate error %v", key))
	}
	uniqueErrors[key] = message

	return &er{
		code:    codes.Internal,
		message: message,
		errorInfo: &errdetails.ErrorInfo{
			Domain:   string(key),
			Reason:   message,
			Metadata: make(map[string]string),
		},
	}
}

type er struct {
	langFn    func(ctx context.Context, key string) error
	code      codes.Code
	message   string
	errorInfo *errdetails.ErrorInfo
}

func (e *er) WithCode(code codes.Code) Error {
	e.code = code
	return e
}

func (e *er) WithInfo(reason string, metadata ...interface{}) Error {
	e.errorInfo.Reason = reason
	key := ""
	for i, v := range metadata {
		if i%2 == 0 { //key
			key = fmt.Sprintf("%v", v)
		} else { //value
			e.errorInfo.Metadata[key] = fmt.Sprintf("%v", v)
		}
	}
	return e
}

func (e *er) Error() string {
	return fmt.Sprintf("%s %v", e.message, e.errorInfo.GetMetadata())
}

func (e *er) ToGRPC(ctx context.Context) *status.Status {
	// Exclude grpc er
	if e.code == codes.OK {
		e.code = codes.Internal
	}
	st := status.New(e.code, e.message)

	localeDetail := &errdetails.LocalizedMessage{}
	if langFn != nil {
		localeDetail.Locale, localeDetail.Message = langFn(ctx, e.errorInfo.Domain)
	}
	if len(localeDetail.Message) == 0 {
		localeDetail.Locale = DefaultLocale
		localeDetail.Message = e.message
		for k, v := range e.errorInfo.GetMetadata() {
			k = "{" + k + "}"
			localeDetail.Message = strings.Replace(localeDetail.Message, k, v, -1)
		}
	}
	st, _ = st.WithDetails(localeDetail)

	st, _ = st.WithDetails(e.errorInfo)

	return st
}

func (e *er) FromValidation(err error) Error {
	if err == nil {
		return nil
	}

	validationErr, ok := err.(ov.Errors)
	if !ok {
		e.code = codes.InvalidArgument
		e.message = "validation error: " + err.Error()
		e.errorInfo.Reason = "validation error"
		e.errorInfo.Metadata = map[string]string{"error": err.Error()}
		return e
	}

	if validationErr == nil {
		return nil
	}

	// normalize lngerr
	filtered := filterErrors(validationErr)

	// if present internal error - return Internal error of validator
	if internal := getInternalError(filtered); internal != nil {
		e.code = codes.Internal
		e.message = "validator internal error: " + internal.Error()
		e.errorInfo.Reason = "validator internal error"
		e.errorInfo.Domain = ""
		e.errorInfo.Metadata = map[string]string{"error": internal.Error()}
		return e
	}

	// make info
	e.code = codes.InvalidArgument
	e.errorInfo.Reason = "validation error"
	for field, vErr := range filtered {
		e.errorInfo.Metadata[field] = vErr.Error()
	}

	return e
}

type sliceError []ov.Errors

func (s sliceError) ToStringSlice() []string {
	slice := make([]string, 0, len(s))
	for _, val := range s {
		slice = append(slice, val.Error())
	}

	return slice
}

func (s sliceError) Error() string {
	return strings.Join(s.ToStringSlice(), "")
}

func filterErrors(errors ov.Errors) ov.Errors {
	for key, err := range errors {
		switch val := err.(type) {
		case ov.Errors:
			if len(val) == 0 {
				delete(errors, key)
				continue
			}

			errors[key] = filterErrors(val.Filter().(ov.Errors))
		case sliceError:
			if len(val) == 0 {
				delete(errors, key)
				continue
			}
		}
	}

	return errors
}

func getInternalError(errors map[string]error) error {
	for _, err := range errors {
		switch val := err.(type) {
		case ov.InternalError:
			return val
		case ov.Errors:
			return getInternalError(val)
		case sliceError:
			for _, subErr := range val {
				if internal := getInternalError(subErr); internal != nil {
					return internal
				}
			}
		}
	}

	return nil
}
