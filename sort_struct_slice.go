package utils

import (
	"errors"
	"fmt"
	"reflect"
	"sort"
	"strings"

	"google.golang.org/protobuf/types/known/timestamppb"
)

type orderStruct struct {
	Index int    //индекс поля в структуре
	Name  string //имя поля в Json'е
	Desc  bool   //признак обратной сортировки
}

type orderSlice []orderStruct

func SortStructSlice(structSlice interface{}, order []string, fieldNameFromJson bool) (err error) {

	defer func() {
		if r := recover(); r != nil {
			err = errors.New(fmt.Sprintf("PANIC: %v", r))
		}
	}()

	//сортировку не заказали - на выход
	if len(order) < 1 {
		return
	}

	val := reflect.ValueOf(structSlice)

	if val.Type().Kind() == reflect.Ptr {
		if val.IsNil() {
			return err
		}
		val = reflect.Indirect(val)
	}

	//передали не слайс - ошибка, выход
	if val.Type().Kind() != reflect.Slice {
		err = errors.New(fmt.Sprintf("%s isn't slice", val.Type().Name()))
		return err
	}

	//элемент слайса не структура и не указатель на структуру - ошибка, выход
	if !(val.Type().Elem().Kind() == reflect.Struct) && !(val.Type().Elem().Kind() == reflect.Ptr && val.Type().Elem().Elem().Kind() == reflect.Struct) {
		err = errors.New(fmt.Sprintf("%s isn't struct", val.Type().Elem().Name()))
		return err
	}

	structSlice = val.Interface()

	//определяем индексы полей сортировки
	var orderSlice orderSlice
	for k := 0; k < len(order); k++ {
		orderFieldName := order[k]
		desc := strings.Compare(order[k][:1], "-") == 0
		if desc {
			orderFieldName = orderFieldName[1:]
		}

		fieldIndex := -1
		pv := val.Type().Elem()
		if val.Type().Elem().Kind() == reflect.Ptr {
			pv = pv.Elem()
		}

		for i := 0; i < pv.NumField(); i++ {
			fieldName := ""
			if fieldNameFromJson {
				fieldName = pv.Field(i).Tag.Get("json")
				fieldName = strings.Split(fieldName, ",")[0]
			} else {
				fieldName = pv.Field(i).Name
			}
			if strings.Compare(fieldName, orderFieldName) == 0 {
				fieldIndex = i
				break
			}
		}

		//не нашли заказанное поле для сортировки - ошибка, выход
		if fieldIndex < 0 {
			err = errors.New(fmt.Sprintf("Field %s for sorting not found", orderFieldName))
			return
		}

		orderSlice = append(orderSlice, orderStruct{fieldIndex, orderFieldName, desc})
	}

	//собственно сортировка
	sort.SliceStable(structSlice, func(i, j int) bool {
		var res bool
		for k := 0; k < len(orderSlice); k++ {
			var comp int
			fieldIndex := orderSlice[k].Index

			vs := val.Index(i) // src struct
			if val.Type().Elem().Kind() == reflect.Ptr {
				vs = vs.Elem()
			}
			fs := vs.Field(fieldIndex) // src field

			vd := val.Index(j) // dst struct
			if val.Type().Elem().Kind() == reflect.Ptr {
				vd = vd.Elem()
			}
			fd := vd.Field(fieldIndex) // dest field

			if comp, err = CompareValue(fs, fd); err != nil {
				err = errors.New(fmt.Sprintf("CompareValue error: %s", err.Error()))
				return false
			}
			res = comp < 0

			if orderSlice[k].Desc {
				res = !res
			}

			//если значение полей одинаковое, сравнение сделаем по следующему полю
			if comp == 0 {
				continue
			}
			break
		}
		return res
	})

	return err
}

func CompareValue(lv reflect.Value, rv reflect.Value) (res int, err error) {

	defer func() {
		if r := recover(); r != nil {
			err = errors.New(fmt.Sprintf("PANIC: %v", r))
		}
	}()

	//хотят сравнить значения разного типа - ошибка, выход
	if lv.Kind() != rv.Kind() {
		err = errors.New("left value and right value have different kind")
		return
	}

	switch lv.Kind() {
	case reflect.Ptr:
		if lv.IsNil() && rv.IsNil() {
			res = 0
		} else if lv.IsNil() {
			res = -1
		} else if rv.IsNil() {
			res = 1
		} else {
			return CompareValue(reflect.Indirect(lv), reflect.Indirect(rv))
		}
	case reflect.Bool:
		if lv.Bool() == rv.Bool() {
			res = 0
		} else if lv.Bool() == false {
			res = -1
		} else {
			res = 1
		}
		//less = (lv.Bool() != rv.Bool())&&(lv.Bool() == false)&&(rv.Bool() == true)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		if lv.Int() < rv.Int() {
			res = -1
		} else if lv.Int() > rv.Int() {
			res = 1
		} else {
			res = 0
		}
		//less = lv.Int() < rv.Int()
	case reflect.Uintptr:
		if uint64(lv.Int()) < uint64(rv.Int()) {
			res = -1
		} else if uint64(lv.Int()) > uint64(rv.Int()) {
			res = 1
		} else {
			res = 0
		}
		//less = uint64(lv.Int()) < uint64(rv.Int())
	case reflect.Float32, reflect.Float64:
		if lv.Float() < rv.Float() {
			res = -1
		} else if lv.Float() > rv.Float() {
			res = 1
		} else {
			res = 0
		}
		//less = lv.Float() < rv.Float()
	case reflect.String:
		res = strings.Compare(lv.String(), rv.String())
		//less = lv.String() < rv.String()

	case reflect.Struct:
		switch lv.Type().Name() {
		case "Timestamp":
			var refLeft timestamppb.Timestamp
			var refRight timestamppb.Timestamp
			var ok bool
			refLeft, ok = lv.Interface().(timestamppb.Timestamp)
			if !ok {
				err = errors.New(fmt.Sprintf("cann't approve type Timestamp for struct %s isn't implemented", lv.Type().Name()))
				return
			}

			refRight, ok = rv.Interface().(timestamppb.Timestamp)
			if !ok {
				err = errors.New(fmt.Sprintf("cann't approve type Timestamp for struct %s isn't implemented", rv.Type().Name()))
				return
			}

			if refLeft.Seconds < refRight.Seconds {
				res = -1
			} else if refLeft.Seconds > refRight.Seconds {
				res = 1
			} else {
				if refLeft.Nanos < refRight.Nanos {
					res = -1
				} else if refLeft.Nanos > refRight.Nanos {
					res = 1
				} else {
					res = 0
				}
			}
		default:
			err = errors.New(fmt.Sprintf("comparison values of struct %s isn't implemented", lv.Type().Name()))
			return
		}

	//case reflect.Invalid:
	//case reflect.Complex64:
	//case reflect.Complex128:
	//case reflect.Array:
	//case reflect.Chan:
	//case reflect.Func:
	//case reflect.Interface:
	//case reflect.Map:
	//case reflect.Slice:
	//case reflect.UnsafePointer:
	default:
		err = errors.New(fmt.Sprintf("comparison values of kind %+v isn't implemented", lv.Kind()))
		return
	}

	return
}

func PagginStruct(structSlice interface{}, limit int, offset int) (paggedStruct interface{}, err error) {
	//передали не слайс - ошибка, выход
	val := reflect.ValueOf(structSlice)
	if val.Type().Kind() == reflect.Ptr {
		if val.IsNil() {
			return structSlice, err
		}
		val = reflect.Indirect(val)
	}

	if val.Type().Kind() != reflect.Slice {
		err = errors.New(fmt.Sprintf("%s isn't slice", val.Type().Name()))
		return
	}

	if limit == 0 {
		limit = val.Len()
	}

	var firstElementPosition, lastElementPosition int

	firstElementPosition = offset
	lastElementPosition = firstElementPosition + limit

	if firstElementPosition > val.Len() {
		firstElementPosition = 0
		lastElementPosition = 0
	} else if lastElementPosition > val.Len() {
		lastElementPosition = val.Len()
	}

	//val = val.Slice(firstElementPosition, lastElementPosition)
	newSlice := make([]interface{}, 0, limit)
	for i := firstElementPosition; i < lastElementPosition; i++ {
		newSlice = append(newSlice, val.Index(i).Interface())
	}

	return interface{}(&newSlice), nil
}

func GetLimitOfsetPositions(structSlice interface{}, limit int, offset int) (firstElementPosition, lastElementPosition int, err error) {
	//передали не слайс - ошибка, выход
	val := reflect.ValueOf(structSlice)
	if val.Type().Kind() != reflect.Slice {
		err = errors.New(fmt.Sprintf("%s isn't slice", val.Type().Name()))
		return
	}

	if limit == 0 {
		limit = val.Len()
	}

	firstElementPosition = offset
	lastElementPosition = firstElementPosition + limit

	if firstElementPosition > val.Len() {
		firstElementPosition = 0
		lastElementPosition = 0
	} else if lastElementPosition > val.Len() {
		lastElementPosition = val.Len()
	}

	return
}
