package utils

import (
	"fmt"
	ov "github.com/go-ozzo/ozzo-validation"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strings"
)

var GrpcError = &grpcError{}

type grpcError struct{}

func (t *grpcError) FromValidation(err error) error {
	validationErr, ok := err.(ov.Errors)
	if !ok {
		return err
	}

	if validationErr == nil {
		return nil
	}

	filtered := filterErrors(validationErr)

	if internal := getInternalError(filtered); internal != nil {
		return status.Error(codes.Internal, internal.Error())
	}

	statusResponse, err := badRequest(filtered)
	if err != nil {
		return err
	}

	if statusResponse != nil {
		return statusResponse.Err()
	}

	return nil
}

func filterErrors(errors ov.Errors) ov.Errors {
	for key, err := range errors {
		switch val := err.(type) {
		case ov.Errors:
			if len(val) == 0 {
				delete(errors, key)
				continue
			}

			errors[key] = filterErrors(val.Filter().(ov.Errors))
		case sliceError:
			if len(val) == 0 {
				delete(errors, key)
				continue
			}
		}
	}

	return errors
}

func getInternalError(errors map[string]error) error {
	for _, err := range errors {
		switch val := err.(type) {
		case ov.InternalError:
			return val
		case ov.Errors:
			return getInternalError(val)
		case sliceError:
			for _, subErr := range val {
				if internal := getInternalError(subErr); internal != nil {
					return internal
				}
			}
		}
	}

	return nil
}

func countTotalErrors(errors map[string]error) int {
	count := 0
	for _, err := range errors {
		switch val := err.(type) {
		case ov.Errors:
			count += countTotalErrors(val)
		case sliceError:
			for _, subErr := range val {
				count += countTotalErrors(subErr)
			}
		case error:
			count++
		}
	}

	return count
}

func makeFieldViolations(errors map[string]error, parentKey string, violations []*errdetails.BadRequest_FieldViolation) []*errdetails.BadRequest_FieldViolation {
	for key, err := range errors {
		switch val := err.(type) {
		case ov.Errors:
			violations = makeFieldViolations(val, key, violations)
		case sliceError:
			for subKey, subErr := range val {
				violations = makeFieldViolations(subErr, strings.Join([]string{parentKey, key, fmt.Sprintf("%v", subKey)}, "."), violations)
			}
		case error:
			violations = append(violations, &errdetails.BadRequest_FieldViolation{
				Field:       strings.TrimPrefix(strings.Join([]string{parentKey, key}, "."), "."),
				Description: err.Error(),
			})
		}
	}

	return violations
}

func makeBadRequest(errors map[string]error) *errdetails.BadRequest {
	details := &errdetails.BadRequest{
		FieldViolations: makeFieldViolations(
			errors,
			"",
			make([]*errdetails.BadRequest_FieldViolation, 0, countTotalErrors(errors)),
		),
	}

	return details
}

func badRequest(errors map[string]error) (*status.Status, error) {
	if errors == nil {
		return nil, nil
	}
	response := status.New(codes.InvalidArgument, "bad request")

	statusResponse := makeBadRequest(errors)
	if len(statusResponse.FieldViolations) > 0 {
		return response.WithDetails(statusResponse)
	}

	return nil, nil
}

func (t *grpcError) ExtractFieldErrors(err error) map[string][]error {
	errMap := make(map[string][]error, 0)

	for _, detail := range status.Convert(err).Details() {
		switch t := detail.(type) {
		case *errdetails.BadRequest:
			for _, violation := range t.GetFieldViolations() {
				fieldErrors, ok := errMap[violation.GetField()]
				if ok {
					errMap[violation.GetField()] = append(fieldErrors, fmt.Errorf(violation.GetDescription()))
				} else {
					errMap[violation.GetField()] = []error{fmt.Errorf(violation.GetDescription())}
				}
			}
		}
	}

	if len(errMap) > 0 {
		return errMap
	}

	return nil
}

type sliceError []ov.Errors

func (s sliceError) ToStringSlice() []string {
	slice := make([]string, 0, len(s))
	for _, val := range s {
		slice = append(slice, val.Error())
	}

	return slice
}

func (s sliceError) Error() string {
	return strings.Join(s.ToStringSlice(), "")
}
