package utils

import (
	"fmt"
	"testing"

	ov "github.com/go-ozzo/ozzo-validation"
	"github.com/stretchr/testify/require"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/status"
)

func TestBadRequest(t *testing.T) {
	respStatus, err := badRequest(nil)

	require.Nil(t, respStatus)
	require.NoError(t, err)

	respStatus, err = badRequest(map[string]error{"test": fmt.Errorf("error")})

	require.NoError(t, err)
	require.IsType(t, new(status.Status), respStatus)
}

func TestExtractErrors(t *testing.T) {
	errors := GrpcError.ExtractFieldErrors(nil)

	require.Nil(t, errors)

	respStatus, _ := badRequest(map[string]error{"test": fmt.Errorf("error")})
	errors = GrpcError.ExtractFieldErrors(respStatus.Err())
	require.NotNil(t, errors)
	require.Equal(t, errors["test"][0], fmt.Errorf("error"))
}

func TestFromValidation(t *testing.T) {
	err := GrpcError.FromValidation(nil)

	require.NoError(t, err)

	err = GrpcError.FromValidation(ov.Errors{"test": fmt.Errorf("test")})

	require.Error(t, err)
	require.Equal(t, "rpc error: code = InvalidArgument desc = bad request", err.Error())

	err = GrpcError.FromValidation(ov.Errors{"test": ov.NewInternalError(fmt.Errorf("test"))})

	require.Error(t, err)
	require.Equal(t, "rpc error: code = Internal desc = test", err.Error())
}

func TestGetInternalError(t *testing.T) {
	errors := ov.Errors{
		"ololo":  fmt.Errorf("trololo"),
		"ololo2": fmt.Errorf("trololo"),
		"internal2": ov.Errors{
			"ololo":    fmt.Errorf("trololo"),
			"internal": sliceError{ov.Errors{"field1": ov.NewInternalError(fmt.Errorf("trololo_internal_deep"))}, ov.Errors{"field1": fmt.Errorf("trololo_internal_deep")}},
		},
	}

	err := getInternalError(errors)

	require.Error(t, err)
	require.Equal(t, "trololo_internal_deep", err.Error())

	err = getInternalError(nil)

	require.Nil(t, err)
}

func TestCountTotalErrors(t *testing.T) {
	errors := ov.Errors{
		"ololo":  fmt.Errorf("trololo"),
		"ololo2": fmt.Errorf("trololo"),
		"internal2": ov.Errors{
			"ololo":    fmt.Errorf("trololo"),
			"internal": sliceError{ov.Errors{"field1": fmt.Errorf("trololo_internal_deep")}, ov.Errors{"field1": fmt.Errorf("trololo_internal_deep")}},
		},
	}

	count := countTotalErrors(errors)
	require.Equal(t, 5, count)

	count = countTotalErrors(nil)
	require.Equal(t, 0, count)
}

func TestMakeFieldViolations(t *testing.T) {
	errors := ov.Errors{
		"ololo":  fmt.Errorf("trololo"),
		"ololo2": fmt.Errorf("trololo"),
		"internal2": ov.Errors{
			"ololo": fmt.Errorf("trololo"),
			"internal": sliceError{
				ov.Errors{"field1": fmt.Errorf("trololo_internal_deep")},
				ov.Errors{"field2": fmt.Errorf("trololo_internal_deep")},
			},
		},
	}

	violations := make([]*errdetails.BadRequest_FieldViolation, 0, countTotalErrors(errors))
	val := makeFieldViolations(errors, "", violations)

	require.NotNil(t, val)
	require.Equal(t, 5, len(val))
}

func TestSliceError_ToStringSlice(t *testing.T) {
	err := sliceError{ov.Errors{"test": fmt.Errorf("test")}}

	require.Error(t, err)
	require.Equal(t, []string{"test: test."}, err.ToStringSlice())
}

func TestSliceError_Error(t *testing.T) {
	err := sliceError{ov.Errors{"test": fmt.Errorf("test")}, ov.Errors{"test": fmt.Errorf("test")}}

	require.Error(t, err)
	require.Equal(t, "test: test.test: test.", err.Error())
}
