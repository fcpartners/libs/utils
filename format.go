package utils

import (
	"reflect"
	"strconv"
)

func GetNameByInterface(value interface{}) string {
	var res string

	if value == nil {
		value = "nil"
	}

	switch v := value.(type) {
	case string:
		res = v
	case []byte:
		res = string(v)
	case bool:
		res = strconv.FormatBool(v)
	case float32:
		res = strconv.FormatFloat(float64(v), 'f', 2, 64)
	case float64:
		res = strconv.FormatFloat(v, 'f', 2, 64)
	case int:
		res = strconv.FormatInt(int64(v), 10)
	case int8:
		res = strconv.FormatInt(int64(v), 10)
	case int16:
		res = strconv.FormatInt(int64(v), 10)
	case int32:
		res = strconv.FormatInt(int64(v), 10)
	case int64:
		res = strconv.FormatInt(v, 10)
	case uint:
		res = strconv.FormatUint(uint64(v), 10)
	case uint8:
		res = strconv.FormatUint(uint64(v), 10)
	case uint16:
		res = strconv.FormatUint(uint64(v), 10)
	case uint32:
		res = strconv.FormatUint(uint64(v), 10)
	case uint64:
		res = strconv.FormatUint(v, 10)
	default:
		typ := IndirectType(reflect.TypeOf(value))
		res = typ.Name()
		if res == "" {
			res = reflect.ValueOf(value).Type().String()
		}
	}
	return res
}

func IndirectType(v reflect.Type) reflect.Type {
	switch v.Kind() {
	case reflect.Ptr:
		return IndirectType(v.Elem())
	default:
		return v
	}
}