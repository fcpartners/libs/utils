package utils

import (
	"crypto/rand"

	"github.com/google/uuid"
	"github.com/oklog/ulid"
)

type (
	Generator interface {
		String() string
		Int64() int64
	}
)

type (
	ulidGenerator struct{}
	uuidGenerator struct{}
)

//----------------------------------------------------------------------------------------------------------------------
// UUID - 8fdd05e4-d5d7-4a7d-b99b-0077ed0d69e5
//----------------------------------------------------------------------------------------------------------------------
func UUID() Generator {
	return &uuidGenerator{}
}

func (*uuidGenerator) New() (string, error) {
	uid, err := uuid.NewRandom()
	if err != nil {
		return "", err
	}
	return uid.String(), nil
}

func (*uuidGenerator) String() string {
	return uuid.New().String()
}

func (*uuidGenerator) Int64() int64 {
	return int64(uuid.New().Time())
}

//----------------------------------------------------------------------------------------------------------------------
// ULID - 01DMFH1SXAWBMY7NHEZ6SDQ2Y3
//----------------------------------------------------------------------------------------------------------------------
func ULID() Generator {
	return &ulidGenerator{}
}

func (*ulidGenerator) String() string {
	return ulid.MustNew(ulid.Now(), rand.Reader).String()
}

func (*ulidGenerator) Int64() int64 {
	return int64(ulid.MustNew(ulid.Now(), rand.Reader).Time())
}