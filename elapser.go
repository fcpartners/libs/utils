package utils

import (
	"fmt"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
// ELAPSER
//----------------------------------------------------------------------------------------------------------------------
type elapser struct {
	start time.Time
}

func Elapser() *elapser {
	return &elapser{time.Now()}
}

func (t *elapser) String() string {
	return fmt.Sprintf("[Duration: %s]", time.Since(t.start).String())
}
